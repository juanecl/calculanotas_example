/*
 * Autor: Juan Enrique Chomon
 * 2014/08
 * 
 * Información: #cuenta es un campo de tipo "hidden" que se utiliza para almacenar el total de calificaciones que se han ingresado, sin importar si una o más fueron eliminadas 
 */

var divisor = 100;

//Cálculo de nota por porcentaje ponderado
function ponderacion(calificacion, porcentaje) {
    return calificacion * (porcentaje / divisor);
}

//Cálculo de la nota que hace falta para aprobar
function paraAprobar(residuo, ponderacion_restante) {
    return residuo / ponderacion_restante;
}

//Obtener cantidad de calificaciones "a ingresar" que se han agregado
function obtenerLargo() {
    return $("#cuenta").val();
}

//Función para mostrar mensajes de ayuda, error o datos
function mostrarMensaje(id, mensaje) {
    var img = (id === "ayuda") ? "negro" : "blanco";
    $("#resultados").css("display", "block");
    desvanecer("in", id);
    $("#resultados #" + id).append('<h5><span>' + mensaje + '</span><a href="" class="cerrar" rel="' + id + '"><img src="imagenes/eliminar-' + img + '.png"/></a></h5></br>');
    return true;
}

//Función estética para aparecer y desaparecer objetos con efecto fundido
function desvanecer(efecto, id) {
    $("#" + id).html("");
    if (efecto === "in") {
        $("#" + id).fadeIn();
    } else {
        $("#" + id).fadeOut();
    }
}

//Obtener el valor de una campo de texto
function obtenerItem(item, i) {
    return $("#" + item + i).val();
}

//Resolver el promedio dada las calificaciones y porcentajes ingresados
function resolverPromedio() {
    var i, porcentaje, pond, resultado = 0;
    for (i = 1; i <= obtenerLargo(); i++) {
        porcentaje = obtenerItem("porcentaje", i);
        if (porcentaje === "" || isNaN(porcentaje) || porcentaje === undefined) {
            porcentaje = 0;
        } else {
            pond = ponderacion(porcentaje, obtenerItem("calificacion", i));
            resultado += pond;
        }
    }
    return resultado;
}

//Calcular el porcentaje total acumulado que se ha ingresado
function resolverPonderacion() {
    var porcentaje, i, pond = 0;

    for (i = 1; i <= obtenerLargo(); i++) {
        porcentaje = obtenerItem("porcentaje", i);
        if (porcentaje === "" || isNaN(porcentaje) || porcentaje === undefined) {
            porcentaje = 0;
        } else {
            porcentaje /= divisor;
        }
        pond += porcentaje;
    }
    return pond;
}

//Vaciar campos de texto
function vaciarInputs() {
    for (var i = 1; i <= obtenerLargo(); i++) {
        if ($("#calificacion" + i).length > 0 || $("#porcentaje" + i).length > 0) {
            $("#calificacion" + i).val("");
            $("#porcentaje" + i).val("");
        }
    }
    return true;
}

$(document).ready(function() {
    //Establecer largo máximo para los campos de texto
    $(".porcentaje").attr('maxlength', 3);
    $(".calificacion").attr('maxlength', 3);

    $("#borrar").click(function() {
        vaciarInputs();
        location.reload();
    });

    //Botón para agregar una nueva calificación
    $("#agregar_calificacion").click(function(e) {
        e.preventDefault();
        var num = 0;

        num = parseInt($("#cuenta").val()) + 1;
        $("#cuenta").val(num);

        $("#lista").append('<li class="item' + num + '"><input type="text" class="calificacion" id="calificacion' + num + '" placeholder="Calificación"/><input type="text" id="porcentaje' + num + '" class="porcentaje" placeholder="Porcentaje"/><a href="" id="eliminar_calificacion" rel="' + num + '"><img src="imagenes/delete.png" /></a></li>');
        $(".porcentaje").attr('maxlength', 3);
        $(".calificacion").attr('maxlength', 3);
    });

    //Botón para eliminar una calificación
    $(document).on("click", "#eliminar_calificacion", function(e) {
        e.preventDefault();
        var num = $(this).attr("rel");
        $(".item" + num).remove();
    });

    //Filtro para evitar valores de porcentaje fuera de rango
    $(document).on("change", ".porcentaje", function() {
        var valor = $(this).val();
        var id, mensaje;
        if (valor.length === 3) {
            if (parseInt(valor) > divisor) {
                id = "ayuda";
                mensaje = 'El valor no debe ser superior a 100';
                mostrarMensaje(id, mensaje);
                $(this).val("");
            }
        }
    });

    //Filtro para dar formato a la calificación y evitar valores fuera de rango
    $(document).on("keyup", ".calificacion", function() {
        var valor = $(this).val();
        var arr = valor.split(".");
        var entero, id, mensaje, decimal;

        if (valor.length > 1 && arr.length <= 1) {
            entero = valor.charAt(0);
            if (entero > 7) {
                id = "ayuda";
                mensaje = 'El valor del entero no debe ser superior a 7';
                mostrarMensaje(id, mensaje);
                $(this).val("");
            } else {
                if (parseInt(entero) === 7) {
                    decimal = valor.charAt(1);
                    if (decimal > 0) {
                        id = "ayuda";
                        mensaje = 'El valor del decimal, cuando el entero es ' + entero + ' no debe ser superior a 0';
                        mostrarMensaje(id, mensaje);
                        $(this).val("");
                    } else {
                        valor = valor.charAt(0) + "." + valor.charAt(1);
                        $(this).val(valor);
                    }
                } else {
                    valor = valor.charAt(0) + "." + valor.charAt(1);
                    $(this).val(valor);
                }
            }
        }
    });

    //Botón para resolver promedio incompleto acumulado
    $("#resolver_promedio_parcial").click(function(e) {
        e.preventDefault();
        var ponderacion_restante, promedio_acumulado, nota_aprobar, residuo, i, mensaje, dif;

        ponderacion_restante = (divisor - (resolverPonderacion() * divisor)).toFixed(0);

        promedio_acumulado = resolverPromedio();

        residuo = 3.95 - promedio_acumulado;

        if (residuo > 0) {
            nota_aprobar = paraAprobar(residuo, ponderacion_restante / divisor);
            nota_aprobar = Math.ceil(nota_aprobar * divisor) / divisor;

            if (parseInt(nota_aprobar.toString().charAt(nota_aprobar.toString().length - 1)) > 0) {
                dif = 10 - (parseInt(nota_aprobar.toString().charAt(nota_aprobar.toString().length - 1)));
                dif /= divisor;
                nota_aprobar += dif;
            }

            i = "datos";
            mensaje = "La calificación que necesitas para el restante " + ponderacion_restante + "% es: " + nota_aprobar.toFixed(1);
            mostrarMensaje(i, mensaje);

            i = "ayuda";
            mensaje = "La nota acumulada es: " + promedio_acumulado.toFixed(2);
            mostrarMensaje(i, mensaje);
        } else {
            if (isNaN(residuo) || residuo === undefined || residuo === "") {

                desvanecer("out", "datos");

                i = "errores";
                mensaje = "Debe ingresar valores válidos para realizar esta operación";
                mostrarMensaje(i, mensaje);

                i = "ayuda";
                mensaje = 'En caso que este error siga apareciendo, haga clic en "Borrar".';
                mostrarMensaje(i, mensaje);

            } else {

                desvanecer("out", "errores");
                desvanecer("out", "ayuda");

                i = "datos";
                mensaje = "Felicidades ya aprobaste, llevas un: " + promedio_acumulado.toFixed(2) + " de promedio. Ya puedes respirar tranquilo/a";
                mostrarMensaje(i, mensaje);

                vaciarInputs();
            }
        }
    });

    //Botón para resolver el promedio completo
    $("#resolver_promedio").click(function(e) {
        e.preventDefault();
        var pond = resolverPonderacion();
        var i, residuo, mensaje, promedio = 0;

        if (pond < 1) {
            residuo = (1 - pond);
            residuo *= divisor;
            residuo = residuo.toFixed(0);

            desvanecer("out", "datos");

            i = "errores";
            mensaje = 'La ponderación del promedio está incompleta, falta un <strong>' + residuo + '%</strong> de la calificación final';
            mostrarMensaje(i, mensaje);

            i = "ayuda";
            mensaje = 'Para hacer un promedio de la ponderación acumulada incompleta, haga clic en el botón "<strong>Promedio parcial</strong>"';
            mostrarMensaje(i, mensaje);

        } else if (pond === 1) {

            desvanecer("out", "errores");
            desvanecer("out", "ayuda");
            promedio = resolverPromedio();

            i = "datos";
            mensaje = 'Tu promedio de notas es: ' + promedio.toFixed(1);
            mostrarMensaje(i, mensaje);

            var id = $("#resultados #datos h5");
            if (promedio.toFixed(2) < 3.95) {
                if (id.hasClass("aprobado")) {
                    id.removeClass("aprobado");
                }
                id.addClass("reprobado");
            } else {
                if (id.hasClass("reprobado")) {
                    id.removeClass("reprobado");
                }
                id.addClass("aprobado");
            }

            vaciarInputs();

        } else if (pond > 1) {
            desvanecer("out", "datos");
            id = "errores";
            mensaje = 'La ponderación del promedio supera el 100%, por favor revise los porcentajes';
            mostrarMensaje(id, mensaje);
        }
    });

    //Botón para cerrar bloque de ayuda, error o dato
    $(document).on("click", "a.cerrar", function(e) {
        e.preventDefault();
        var rel = $(this).attr("rel");
        $("#resultados #" + rel).fadeOut();
    });
});